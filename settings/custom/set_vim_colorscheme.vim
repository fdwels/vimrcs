"CUSTOM CHANGES TO COLOR SCHEMEs:
"
"	This part is entirely personal, and so on,
"	 I'm not giving applying these changes to cterm
"

"	ALL: should work for all dark colors schemes

function! Dw_Cs_FixAll() abort
	hi CursorLineNr    gui=italic
	hi CursorLine      guibg=#202020
	hi vimCommentTitle gui=italic,bold
	hi Comment         gui=italic
endfunction

"	SPECIFIC: specific changes to specific colors schemes

function! Dw_Cs_FixSrcery() abort
	hi CursorLineNr    gui=italic
	hi CursorLine      guibg=#282828
endfunction

"	To avoid defining autocmds twice use augroup
augroup Dw_Cs_Fix
	autocmd!
	autocmd ColorScheme *
				\ call Dw_Cs_FixAll()

	autocmd ColorScheme srcery
				\ call Dw_Cs_FixSrcery()
augroup end

