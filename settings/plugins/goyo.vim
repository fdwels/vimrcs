" Goyo: config file

let g:goyo_width=&colorcolumn + 5

nnoremap <Leader>g :Goyo<CR>

"	More tweaks

function! s:goyo_enter()
	setl list
endfunction
"
" function! s:goyo_leave()
" 	nnoremap <M-w> <C-w>
" endfunction
"
autocmd! User GoyoEnter nested call <SID>goyo_enter()
" autocmd! User GoyoLeave nested call <SID>goyo_leave()

