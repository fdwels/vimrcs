" NORMAL MODE MAPPINGS:

"	Key To Key:

"		Select all
nnoremap <C-a> ggVG

"		Switching behaviour
nnoremap P p
nnoremap p P
nnoremap <C-o> <C-i>
nnoremap <C-i> <C-o>

"		For consistency
nnoremap Y y$
nnoremap U <C-r>

"	Key To Command:

"	Clears hlsearch and the command line
nnoremap <Space> :nohlsearch<Bar>:echo<CR>

" WITH SPECIAL KEY: ¿, like g but with ¿
"
"	If you want to use ¿ on mappings, use
"	 recursive mappings ([mode]map)
"

"		Enter Select Mode
nnoremap gh <Nop>
nnoremap gH <Nop>
nnoremap g<C-h> <Nop>

nnoremap ¿s gh
nnoremap ¿S gH
nnoremap ¿<C-s> g<C-h>

"		Enter Replace Mode
nnoremap R <Nop>
nnoremap gR <Nop>

nnoremap ¿r R
nnoremap ¿R gR

"		Toogle (on and off) relative numbers
nnoremap ¿¿ <Cmd>set relativenumber!<CR>

"		Toogle (on and off) list (show listchars)
nnoremap ¿l <Cmd>set list!<CR>

