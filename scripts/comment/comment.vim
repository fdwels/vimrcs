" Comment In Vim: It just needs `filetype plugin on`
"
"	Given that is a workaround, it's needed to
"	 defined b:line_comment in ~/.vim/after/lang.vim
"	eg.:
"	 let b:line_comment="\""
"

" Define function
function! Dw_Comment(comment) abort
	if a:comment
		if !exists("b:line_comment")
			echohl WarningMsg
			echo "ERROR: variable b:line_comment is null."
			echohl None
		else
			exe "normal 0i" . b:line_comment . " "
		endif
	else
		exe "normal 0i" | exe "normal xx"
	endif
endfunction

" Define mappings
"	Comment:
noremap <C-k><C-c> :call Dw_Comment(1)<CR>

"	Uncomment:
noremap <C-k><C-u> :call Dw_Comment(0)<CR>

